﻿using Admin.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Services;
using System.Web.Security;

namespace Admin.Controllers
{

	public class HomeController : Controller
	{
        private ApplicationDbContext db = new ApplicationDbContext();

		[Authorize]
		public ActionResult Index()
		{
			return View();
		}

		[Authorize(Roles = "Administrator, Agent")]
		public ActionResult Dashboard()
		{
			return View();
		}

		[Authorize(Roles = "Administrator, Agent")]
		public ActionResult About()
		{
			ViewBag.Message = "Your application description page.";

			return View();
		}

        [Authorize(Roles = "Administrator")]
        public ActionResult allTask()
        {

            TaskViewModel myTask = new TaskViewModel();


            ViewBag.myTask = myTask;
            ViewBag.Message = "Create Task";
            return View(db.TaskViewModels.ToList());
        }

        public ActionResult Create()
        {

            List<string> appUser = new List<string>();
            var Usr = db.Users.ToList();
            
            if (Usr.Count > 0)
            {
                foreach (var item in Usr)
                {
                    appUser.Add(item.UserName + " (" + item.Lastname + " " + item.Firstname + " " + item.Middlename + " )");
                }
                ViewBag.firstnameList = new SelectList(appUser);
            }
            else
            {
                
                List<string> user = new List<string>();
                user.Add("All");
                ViewBag.firstnameList = new SelectList(user);
            }

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(TaskViewModel taskViewModel)
        {
            if (ModelState.IsValid)
            {
                taskViewModel.dateCreated = System.DateTime.Now;
                db.TaskViewModels.Add(taskViewModel);
                db.SaveChanges();
                return RedirectToAction("allTask");
            }

            return View(taskViewModel);
        }

        // GET: myModels/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TaskViewModel myModel = db.TaskViewModels.Find(id);
            if (myModel == null)
            {
                return HttpNotFound();
            }
            return View(myModel);
        }

        // POST: myModels/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,name,address,age")] TaskViewModel taskViewModel)
        {
            if (TryUpdateModel(taskViewModel))
            {   
                db.Entry(taskViewModel).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(taskViewModel);
        }

        public ActionResult AssignTask(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TaskViewModel taskViewModel = db.TaskViewModels.Find(id);

            if (taskViewModel == null)
            {
                return HttpNotFound();
            }

            //gets user role with user type "user"
            var userRoles = db.Users.Where(u=>u.UserType =="user");

            ViewBag.usersUsername = userRoles.Select(v=>v.Firstname).ToList();
            ViewBag.TaskId = id;
            return View();
        } 

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AssignTask()
        {   


            return View();
        }
    }
}